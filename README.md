## General information

This project is a timetable viewer. Just enter login of an UTC student and get his timetable.

If you don't have any result after typing login using chrome, please install this [chrome module](https://chrome.google.com/webstore/detail/moesif-orign-cors-changer/digfbfaphojjndkpccljibejjbppifbc).

## Set up

First, clone project : 

### `git clone https://gitlab.utc.fr/lebrunan/sr03-dev4.git`

Then install required modules : 

### `npm install`

Then use this command and to run app in development mode :

### `npm start`


Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

## Deploy

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />

