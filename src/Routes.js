import {Route, Switch} from "react-router-dom";
import Main from "./pages/Main";
import Home from "./pages/Home";
import React from "react";

class Routes extends React.Component {
    render() {
        return (
            <Switch>
                <Route
                    exact path="/timetable"
                    component={Main}
                />
                <Route
                    exact path=""
                    component={Home}
                />
            </Switch>
        )
    }
}
export default Routes;