import React from "react";
import InputGroup from "react-bootstrap/InputGroup";
import FormControl from "react-bootstrap/FormControl";
import {Button} from "react-bootstrap";

class InputLogin extends React.Component {

    constructor(props) {
        super(props);
        this.state = {value: ''};

        this.handleChange = this.handleChange.bind(this);

    }

    handleChange(event) {
        this.setState({
            value: event.target.value
        })
    }

    handleKeyDown = (e) => {
        if (e.key === 'Enter')
            this.searchUser();
    };

    searchUser = async () => {
        this.props.searchUser(this.state.value);
    };

    render() {
        return (
            <div className="InputLogin">
                <InputGroup className="mb-3">
                    <InputGroup.Prepend>
                        <InputGroup.Text id="basic-addon1">Identifiant</InputGroup.Text>
                    </InputGroup.Prepend>
                    <FormControl
                        onKeyDown={this.handleKeyDown}
                        onChange={this.handleChange}
                        placeholder="exemple : jbarthel"
                        aria-label="Identifiant"
                        aria-describedby="basic-addon1"
                    />
                    <InputGroup.Append>
                        <Button onClick={this.searchUser}>Rechercher</Button>
                    </InputGroup.Append>
                </InputGroup>
            </div>
        );
    }
}

export default InputLogin;
