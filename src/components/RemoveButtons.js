import React from "react";
import {Button} from "react-bootstrap";
import Modal from "react-bootstrap/Modal";

class RemoveButtons extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            shouldModalBeShown: false,
            loginToRemove: null
        };
    }

    closeModal = () => {
        this.setState({
            shouldModalBeShown: false
        });
    };

    removeLogin = async () => {
        await this.props.removeLogin(this.state.loginToRemove);
        this.setState({
            shouldModalBeShown: false
        });
    };

    showModal = async (e) => {
        this.setState({
            shouldModalBeShown: true,
            loginToRemove: e.target.value
        });
    };

    render() {
        const buttons = this.generateRemoveButtons();
        return (
            <div className="LoginList">
                {buttons}
                <Modal show={this.state.shouldModalBeShown} onHide={this.closeModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>Supprimer</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>Voulez-vous vraiment retirer {this.state.loginToRemove} ? </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.closeModal}>
                            Annuler
                        </Button>
                        <Button variant="primary" onClick={this.removeLogin}>
                            Supprimer
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }

    generateRemoveButtons() {
        let buttons = [];
        this.props.loginList.forEach(login => {
            buttons.push(
                    <Button
                        key={login}
                        value={login}
                        onClick={e => this.showModal(e, "value")}
                    >
                        Retirer {login}
                    </Button>
            );
        });
        return buttons;
    }
}

export default RemoveButtons;
