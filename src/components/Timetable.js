import React from "react";
import {classColors} from "../util/ClassColor"
class Timetable extends React.Component {


    constructor(props) {
        super(props);

        this.classSessionList = null;
    }

    render() {
        if (this.canvas) {
            this.printBaseTimeTable();
        }
        if (this.props.timetables) {
            for (let i=0 ; i< this.props.timetables.length ; i++) {
                    this.classSessionList = this.props.timetables[i];
                    this.printClassSessions(i);
                }
        }
        return null;
    }

    componentDidMount() {
        this.canvas = this.props.getCanvas();
    }

    //region timetableContent
    printClassSessions(index) {
        // check canvas is initialized
        if (this.canvas) {
            this.assignColorToClassSession();
            for (let i=0; i<this.classSessionList.length; i++) {
                this.printClassSession(this.classSessionList[i], index);
            }

        }
    }

    assignColorToClassSession() {
        const classColorsCopy = JSON.parse(JSON.stringify(classColors));
        let uvList = [];
        for (let i=0; i<this.classSessionList.length; i++) {
            let uv = this.classSessionList[i].uv;
            if (! uvList[uv]) {
                uvList[uv] = classColorsCopy.pop();
            }
            this.classSessionList[i].color = uvList[uv];
        }
    }

    printClassSession(classSession, index) {
        this.canvas.fillStyle = classSession.color;
        this.printClassSessionRectangle(classSession, index);
        this.canvas.fillStyle = 'black';
        this.printClassSessionText(classSession, index);
    }

    printClassSessionText(classSession, index) {
        const fontsize = 14;
        this.canvas.font = fontsize - 1 + 'px arial';

        this.printClassSessionHours(classSession, index, fontsize);
        this.printClassSessionTitleAndType(classSession, index, fontsize);
        this.printClassSessionRoomAndGroup(classSession, index, fontsize);
    }

    printClassSessionHours(classSession, index, fontsize) {
        if (this.props.timetables.length < 2) {
            this.canvas.fillText(
                classSession.begin + " - " + classSession.end,
                this.dayToAbscissaValue(classSession, index),
                this.timeToOrdinateValue(classSession.begin) + fontsize);
        }
    }

    printClassSessionTitleAndType(classSession, index, fontsize) {
        if (this.props.timetables.length < 2) {
            this.canvas.fillText(
                classSession.type + " - " + classSession.uv,
                this.dayToAbscissaValue(classSession, index),
                this.timeToOrdinateValue(classSession.begin) + fontsize * 2);
        } else {
            this.canvas.fillText(
                classSession.uv,
                this.dayToAbscissaValue(classSession, index),
                this.timeToOrdinateValue(classSession.begin) + fontsize);
            this.canvas.fillText(
                classSession.type,
                this.dayToAbscissaValue(classSession, index),
                this.timeToOrdinateValue(classSession.begin) + fontsize * 2);
        }
    }

    printClassSessionRoomAndGroup(classSession, index, fontsize) {
        if (this.props.timetables.length < 2) {
            this.canvas.fillText(
                classSession.room + " - " + classSession.group,
                this.dayToAbscissaValue(classSession, index),
                this.timeToOrdinateValue(classSession.begin) + fontsize * 3);
        }
    }

        printClassSessionRectangle(classSession, index) {
        this.canvas.fillRect(
            this.dayToAbscissaValue(classSession, index),
            this.timeToOrdinateValue(classSession.begin),
            this.computeWidth(),
            this.durationToHeight(classSession)
        );
    }

    durationToHeight(classSession) {
        const duration = this.getClassSessionDuration(classSession);
        return duration / 1.2;
    }

    getClassSessionDuration(classSession) {
        return this.timeToMinute(classSession.end) - this.timeToMinute(classSession.begin);
    }

    timeToOrdinateValue(begin) {
        const timeAsMinute = this.timeToMinute(begin);
        const eightAsMinute = 480;
        return 100 + (timeAsMinute - eightAsMinute) / 1.2;
    }

    timeToMinute(begin) {
        const timeAsArray = begin.split(':');
        const hoursAsMinute = timeAsArray[0] * 60;
        return hoursAsMinute + parseInt(timeAsArray[1]);
    }

    dayToAbscissaValue(classSession, index) {
        let value;
        switch(classSession.day) {
            case 'LUNDI' :
                value = 400;
                break;
            case 'MARDI' :
                value = 600;
                break;
            case 'MERCREDI' :
                value = 800;
                break;
            case 'JEUDI' :
                value = 1000;
                break;
            case 'VENDREDI' :
                value = 1200;
                break;
            case 'SAMEDI' :
                value = 1400;
                break;
            default :
                value = -100;
                break;
        }
        return value + index * (200 / this.props.timetables.length);
    }

    computeWidth() {
        return 200/this.props.timetables.length;
    }
    //endregion

    //region BaseTimetable

    printBaseTimeTable() {
        this.printBackground();
        this.printHours();
        this.printDays();
    }

    printBackground() {
        let squareCpt=0;
        let i;
        for(i=1; i<14; i++) {
            let j;
            for (j=1; j<8; j++) {
                //alternate colors
                if(squareCpt%2 === 1) {
                    this.canvas.fillStyle = "#282c34";
                } else {
                    this.canvas.fillStyle = "#3F404D";
                }
                squareCpt++;
                this.canvas.fillRect(200*j,i*50,200,50);
            }
        }
    }

    printHours() {
        this.canvas.fillStyle = "white";
        this.canvas.font = '15px arial';
        this.canvas.fillText("Heure", 275, 80);

        let i;
        for (i=1; i<13; i++) {
            let start = i+7;
            let end = start+1;
            this.canvas.fillText(start + "h - " + end + "h", 275, 50*i + 80);
        }
    }

    printDays() {
        this.canvas.fillText("Lundi", 480, 80);
        this.canvas.fillText("Mardi", 680, 80);
        this.canvas.fillText("Mercredi", 870, 80);
        this.canvas.fillText("Jeudi", 1080, 80);
        this.canvas.fillText("Vendredi", 1265, 80);
        this.canvas.fillText("Samedi", 1470, 80);

    }

    //endregion
}

export default Timetable;
