import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './static/css/style.css';
import {BrowserRouter} from "react-router-dom";
import Routes from "./Routes";

ReactDOM.render(
    <BrowserRouter>
        <Routes/>
    </BrowserRouter>,

    document.getElementById('root')
);
