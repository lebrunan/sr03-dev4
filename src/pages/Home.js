import React from 'react';
import logo from '../static/pictures/watch.png';
import Button from 'react-bootstrap/Button';
import {Link} from "react-router-dom";

class Home extends React.Component {

    render() {
        return (

            <header className="main home">
                <img src={logo} className="App-logo" alt="logo"/>
                <p>
                    Bienvenue sur votre emploi du temps !
                </p>
                <Link to="timetable">
                    <Button variant="dark">
                        Découvrir votre EDT
                    </Button>
                </Link>
            </header>
        );
    }
}


export default Home;
