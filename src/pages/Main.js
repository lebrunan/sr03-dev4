import React from "react";
import InputLogin from '../components/InputLogin';
import Timetable from "../components/Timetable";
import Cookies from 'universal-cookie';
import RemoveButtons from "../components/RemoveButtons";

const cookies = new Cookies();

class Main extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            timetables:  [],
            registeredLoginList: [],
            isIdWrong: false,
            isIdAlreadyBind: false,
            hasCanvasBeenInitialized: false
        };
        this.canvasRef = React.createRef();

}
    render() {
        return (
            <div className="main">
                <InputLogin searchUser={this.searchUser}/>
                {this.isLoginCorrect()}
                <canvas ref={this.canvasRef}  height={700}/>
                <Timetable
                    timetables={this.state.timetables}
                    getCanvas={this.getCanvas}
                />
                <RemoveButtons
                    loginList={this.state.registeredLoginList}
                    removeLogin={this.removeLogin}
                />
            </div>
            );
    }

    getCanvas = () => {
        let context = this.canvasRef.current.getContext('2d');

        if (! this.state.hasCanvasBeenInitialized) {
            this.setState({
                hasCanvasBeenInitialized: true
            });

            let canvas = this.canvasRef.current;
            let dpi = window.devicePixelRatio;
            let style_height = +getComputedStyle(canvas).getPropertyValue("height").slice(0, -2);
            let style_width = +getComputedStyle(canvas).getPropertyValue("width").slice(0, -2);
            canvas.setAttribute('height', style_height * dpi);
            canvas.setAttribute('width', style_width * dpi);
        }
        return context;
    };

    removeLogin = (login) => {
        for( let i = 0; i < this.state.registeredLoginList.length; i++) {
            if ( this.state.registeredLoginList[i] === login) {
                this.state.registeredLoginList.splice(i, 1);
            }
            if ( this.state.timetables[i].login === login) {
                this.state.timetables.splice(i, 1);
            }
            this.setState({
                registeredLoginList: this.state.registeredLoginList,
                timetables: this.state.timetables
            });
        }
        this.setCookies();
    };

    isLoginCorrect() {
        if(this.state.isIdWrong) {
            return <p className="errorMessage">Identifiant incorrect</p>
        } else if (this.state.isIdAlreadyBind) {
            return <p className="errorMessage">Identifiant déjà inscrit</p>
        }
        return null;
    }

    componentDidMount() {
        let logins = cookies.get('registeredLoginList');
        if (logins) {
            logins.forEach(login => {
                this.searchUser(login);
            });
        }
    }

    searchUser = async (value) => {
            await fetch("https://webapplis.utc.fr/Edt_ent_rest/myedt/result/?login=" + value, {
                method: 'GET'
            })
                .then(res => res.json())
                .then(
                    (result) => {

                        // If login doesn't match existing one
                        if (!result.length) {
                            this.setState({
                                isIdWrong: true,
                                isIdAlreadyBind: false
                            })
                        // If login has already been registered
                        } else if (this.state.registeredLoginList.includes(value)) {
                            this.setState({
                                isIdAlreadyBind: true,
                                isIdWrong: false
                            })
                        } else {
                            this.state.registeredLoginList.push(result.login = value);
                            result.login = value;
                            this.state.timetables.push(result);
                            this.setState({
                                registeredLoginList: this.state.registeredLoginList,
                                timetables: this.state.timetables,
                                isIdWrong: false,
                                isIdAlreadyBind: false
                            });

                        }
                        })
                .catch(console.log);
            this.setCookies();
    };

    setCookies() {
        cookies.set("registeredLoginList", this.state.registeredLoginList, {path:'/'});
    }
}
export default Main;